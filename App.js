import React from 'react';
import { AppLoading } from 'expo';
import { Container, Text, Header, Tab, Tabs, Left, Body, Right, Button, Icon, Title, TabHeading, Content, Thumbnail, List, ListItem, View, Fab, Separator, Badge } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import CameraPage from './camera';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }

  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }

    return (
      <Container>
        <Header androidStatusBarColor="#075E54" style={{ backgroundColor: "#128C7E" }}>
          <Body style={{ padding: 10 }}>
            <Title style={{ fontWeight: "bold", fontSize:20 }}>WhatsApp</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name='search' />
            </Button>
            <Button transparent>
              <Icon name='more' />
            </Button>
          </Right>
        </Header>

        <Tabs tabContainerStyle={{ elevation:0 }}>
          <Tab heading={<TabHeading style={{ backgroundColor: "#128C7E" }}><Icon name="camera" /></TabHeading>}>
            <CameraPage />
          </Tab>
          <Tab heading={<TabHeading style={{ backgroundColor: "#128C7E" }}><Text style={{ fontWeight: "bold" }}>CHATS</Text></TabHeading>}>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <Thumbnail source={{ uri: "https://cdn.jitunews.com/dynamic/images/2019/03/natsu_24b497f4df16df3dfefd3717c79a332a.png?w=800"}} />
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>Natsu</Text>
                  <Text note>Doing what you like will always keep you happy . .</Text>
                </Body>
                <Right>
                  <Text note>3:43 pm</Text>
                </Right>
              </ListItem>
            </List>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <Thumbnail source={{ uri: "https://www.anime-planet.com/images/characters/lucy-heartfilia-1416.jpg?t=1538763704" }} />
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>Lucy</Text>
                  <Text note>Doing what you like will always keep you happy . .</Text>
                </Body>
                <Right>
                  <Text note>3:43 pm</Text>
                </Right>
              </ListItem>
            </List>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <Thumbnail source={{ uri: "https://vignette.wikia.nocookie.net/fairytail/images/d/db/Laxus_profile_image.png/revision/latest?cb=20150613141547&path-prefix=id" }} />
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>Laxus</Text>
                  <Text note>Doing what you like will always keep you happy . .</Text>
                </Body>
                <Right>
                  <Text note>3:43 pm</Text>
                </Right>
              </ListItem>
            </List>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <Thumbnail source={{ uri: "https://i.pinimg.com/236x/00/68/4f/00684f9b8fe40a1bbf094045f3f1452d--fairy-tail-erza-scarlet-welt.jpg" }} />
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>Erza</Text>
                  <Text note>Doing what you like will always keep you happy . .</Text>
                </Body>
                <Right>
                  <Text note>3:43 pm</Text>
                </Right>
              </ListItem>
            </List>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <Thumbnail source={{ uri: "https://vignette.wikia.nocookie.net/fairytail/images/d/d1/Mirajane_proposal.png/revision/latest?cb=20140523034537&path-prefix=id" }} />
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>Mirajane</Text>
                  <Text note>Doing what you like will always keep you happy . .</Text>
                </Body>
                <Right>
                  <Text note>3:43 pm</Text>
                </Right>
              </ListItem>
            </List>
            <View style={{ flex: 1 }}>
              <Fab
                containerStyle={{}}
                style={{ backgroundColor: '#25D366' }}
                position="bottomRight">
                <Icon name="chatboxes" />
              </Fab>
            </View>
          </Tab>
          <Tab heading={<TabHeading style={{ backgroundColor: "#128C7E" }}><Text style={{ fontWeight: "bold" }}>STATUS</Text></TabHeading>}>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <View style={{ position: "relative", padding: 1.5 }}>
                    <Thumbnail source={{ uri: "https://cdn.jitunews.com/dynamic/images/2019/03/natsu_24b497f4df16df3dfefd3717c79a332a.png?w=800" }} style={{ width: 54, height: 54 }} />
                    <Badge style={{ width:22, height:21, display:"flex", justifyContent:"center", alignItems:"center",
                      position: "absolute", bottom: 0, right: 0, backgroundColor: '#25D366' }}><Icon name="add" style={{ color:"#ffffff", fontSize:13, fontWeight:"bold" }}></Icon>
                    </Badge>
                  </View>
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>My Status</Text>
                  <Text note>Tap to add status</Text>
                </Body>
              </ListItem>
              <ListItem itemDivider style={{ marginTop: 5 }}>
                <Text>Recent update</Text>
              </ListItem>
              <List>
                <ListItem avatar noBorder>
                  <Left>
                    <View style={{ borderRadius: 50, borderColor:'#25D366', borderWidth:2.5, padding:1.5 }}>
                      <Thumbnail source={{ uri: "https://i.pinimg.com/236x/00/68/4f/00684f9b8fe40a1bbf094045f3f1452d--fairy-tail-erza-scarlet-welt.jpg" }} style={{ width:50, height:50 }} />
                    </View>
                  </Left>
                  <Body>
                    <Text style={{ fontWeight: "bold" }}>Erza</Text>
                    <Text note>Just now</Text>
                  </Body>
                </ListItem>
              </List>
              <ListItem itemDivider style={{ marginTop:5 }}>
                <Text>Viewed status</Text>
              </ListItem>
            </List>
            <List>
              <ListItem avatar noBorder>
                <Left>
                  <View style={{ borderRadius:50, borderColor: 'grey', borderWidth:2.5, padding:1.5 }}>
                    <Thumbnail source={{ uri: "https://vignette.wikia.nocookie.net/fairytail/images/d/db/Laxus_profile_image.png/revision/latest?cb=20150613141547&path-prefix=id" }} style={{ width:50, height:50 }} />
                  </View>
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>Laxus</Text>
                  <Text note>Yesterday, 18:03</Text>
                </Body>
              </ListItem>
            </List>
            <View style={{ flex: 1 }}>
              <Fab
                style={{ backgroundColor: '#F4F4F4', marginBottom:65, height:50, width:50 }}
                position="bottomRight">
                <Icon name="create" style={{ color:"#075E54" }} />
              </Fab>
              <Fab
                containerStyle={{}}
                style={{ backgroundColor: '#25D366' }}
                position="bottomRight">
                <Icon name="camera" />
              </Fab>
            </View>
          </Tab>
          <Tab heading={<TabHeading style={{ backgroundColor: "#128C7E" }}><Text style={{ fontWeight: "bold" }}>CALLS</Text></TabHeading>}>
            <List>
              <ListItem thumbnail>
                <Left>
                  <Thumbnail source={{ uri: "https://www.anime-planet.com/images/characters/lucy-heartfilia-1416.jpg?t=1538763704" }} />
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>Lucy</Text>
                  <Text note numberOfLines={1}><Icon name="redo" style={{ color: 'green' }}></Icon> &nbsp; &nbsp;January 18, 20:20</Text>
                </Body>
                <Right>
                  <Button transparent>
                    <Icon name="call" style={{ color: "#075E54" }}></Icon>
                  </Button>
                </Right>
              </ListItem>
            </List>
            <List>
              <ListItem thumbnail>
                <Left>
                  <Thumbnail source={{ uri: "https://vignette.wikia.nocookie.net/fairytail/images/d/d1/Mirajane_proposal.png/revision/latest?cb=20140523034537&path-prefix=id" }} />
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>Mirajane</Text>
                  <Text note numberOfLines={1}><Icon name="undo" style={{ color: 'red' }}></Icon> &nbsp; &nbsp;January 19, 13:09</Text>
                </Body>
                <Right>
                  <Button transparent>
                    <Icon name="videocam" style={{ color: "#075E54" }}></Icon>
                  </Button>
                </Right>
              </ListItem>
            </List>
            <View style={{ flex: 1 }}>
              <Fab
                containerStyle={{}}
                style={{ backgroundColor: '#25D366' }}
                position="bottomRight">
                <Icon name="call" />
              </Fab>
            </View>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}